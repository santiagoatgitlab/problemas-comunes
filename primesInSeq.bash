from=$1
to=$2
for num in $( seq $1 $2); do
    isPrime=true
    for pDivisor in $( seq 2 $((num / 2)) ); do
        if [ $(("$num" % "$pDivisor")) -eq "0" ]; then
             isPrime=false     
        fi
    done
    if [ "$isPrime" = true ]; then
        echo $num
    fi
done
