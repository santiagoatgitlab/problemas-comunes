module Lib (big) where

import System.Environment

argsToInt :: [String] -> Int
argsToInt a = read (a !! 0)

big :: IO ()
big = do
    args <- getArgs
    putStrLn $ show $length $ show $ (2 ^ (argsToInt args)) - 1
