module Lib ( nugget ) where
import Data.List
import System.Environment

set :: [Int]
set = [6,9,20]

getPossibles :: Int -> [Int]
getPossibles limit = take limit $ foldl1 combine $ map getAllMultiples set
    where combine :: [Int] -> [Int] -> [Int]
          combine list1 list2= nub $ sort $ (+) <$> list1 <*> list2
          getAllMultiples :: Int -> [Int]
          getAllMultiples n = fst $ span (<=limit) $ map (*n) [0..]

nugget :: IO ()
nugget = do
    args <- getArgs
    let limit = read (args !! 0) :: Int
        possibles = getPossibles limit
     in putStrLn $ show $ filter (\n -> not $ elem n possibles) [1..limit]
