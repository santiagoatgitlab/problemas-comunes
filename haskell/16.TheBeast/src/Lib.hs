module Lib (theBeast) where

--                g  g   g  h  h  h  h   // g: greek, h: hebrew  
-- nero cesar
neroCesar = sum [200,60,100,50,6,200,50]
roulette  = sum [1..36]

triangleNumbers = map (\n -> sum [1..n]) [1..50]

theBeast :: IO ()
theBeast = do
    putStrLn $ show [neroCesar, roulette]
    putStrLn $ show triangleNumbers
