module Main where

import Primes
import System.Environment

main :: IO ()
main = do
    args <- getArgs
    let number = read (args !! 0) :: Int
        in putStrLn $ show $ primeFactors number
