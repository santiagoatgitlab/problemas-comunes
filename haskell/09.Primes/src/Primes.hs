module Primes
 ( isPrime
 , primeFactors
 ) where

toFloat i       = (fromIntegral i) :: Float
isPrime pc 
    | pc < 2    = False
    | otherwise = not $ any (\n -> mod pc n == 0 && n /= pc) [2..(ceiling $ sqrt $ toFloat pc)]

firstDivider :: Int -> Int
firstDivider np 
    | np == 1   = 1
    | otherwise = head $ filter (\n -> mod np n == 0) [2..(ceiling $ sqrt $ toFloat np)]

primeFactors :: Int -> [Int]
primeFactors a 
    | a < 2     = error "function primeFactors doesn't accept any number smaller than 2"
    | isPrime a = [a]
    | otherwise = primeFactors factor1 ++ primeFactors factor2
        where factor1 = firstDivider a
              factor2 = round $ (toFloat a) / (toFloat factor1)
