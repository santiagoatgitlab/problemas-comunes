module Lib (pyth) where

import Primes
import Data.Char
import Data.List
import System.Environment

produce :: [Int]
produce = do
    x <- [1..100]
    y <- [1..100]
    if x /= y then [(x^2)+(y^2)] else []

pythPrimes = filter isPrime $ nub $ sort produce

argsToInt :: [String] -> Int
argsToInt (n:_)
    | and $ map isNumber n = read n
    | otherwise = error "Failed trying to parse string to int"

pyth :: IO ()
pyth = do
    args <- getArgs
    putStrLn $ show $ take (argsToInt args) pythPrimes 
