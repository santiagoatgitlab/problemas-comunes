module Lib (smith) where

import Numberphile
import Data.Char

isSmith :: Int -> Bool
isSmith n = (not $ isPrime n) && ((sum $ map digitToInt $ show n) == (sum $ map digitToInt individualFactors))
      where individualFactors = concat $ map show $ primeFactors n

smith :: IO ()
smith = toLines $ map (\s -> (show s) ++ " " ++ (show $ primeFactors s)) $ filter isSmith [2..4937800]
