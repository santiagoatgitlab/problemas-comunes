module Lib (piNumber) where

import System.Environment

data Operation = Add | Sub deriving Eq

operate :: (Num a) => (a, Operation) -> a -> (a, Operation)
operate (n1,o) n2 = let newN = if o == Add then n1+n2 else n1-n2
                        newO = if o == Add then Sub else Add
                    in (newN, newO)

argsToInt :: [String] -> Int
argsToInt []    = 0
argsToInt (s:_) = read s :: Int

myPi prec = (*4) $ fst $ foldl operate (1,Sub) $ map (\n -> 1/n) $ take prec [3,5..]

piNumber :: IO ()
piNumber = do 
        args <- getArgs
        putStrLn "Calculating pi"
        putStrLn $ show $ myPi $ argsToInt args
