module Lib (keith) where

import Data.Char
import Numberphile
import Control.Monad.Writer

limit = 100

keithEngine :: [Int] -> Int -> Int -> Writer [String] Bool
keithEngine (x:xs) orig count = if result == orig
                                then do 
                                    tell [ (show xs) ++ " = " ++ (show result) ]
                                    return True
                                else if count == limit 
                                     then return False
                                     else do
                                         tell [ (show xs) ++ " = " ++ (show result) ]
                                         keithEngine nextList orig (count+1)
                         where result = sum xs
                               nextList = xs ++ [result]

operate :: Int -> (Bool, Int, [String])
operate n = let (isKeith,log) = runWriter $ keithEngine ([0] ++ (map digitToInt $ show n)) n 0
              in (isKeith, n, ["--- " ++ (show n)] ++ log)

isKeith :: (Bool, Int, [String]) -> Bool
isKeith (is, _, _) = is

keithNumbers :: [(Bool, Int, [String])]
keithNumbers = filter isKeith $ map operate [10..99999]

getLog :: (Bool, Int, [String]) -> [String]
getLog (_, _, log) = log

getNumber :: (Bool, Int, [String]) -> Int
getNumber (_, n, _) = n

findMultiples :: [Int] -> Int -> [Int]
findMultiples list n = filter (\pm -> mod pm n == 0) list

filterSingle :: [[Int]] -> [[Int]]
filterSingle = filter (\c -> length c > 1)

lookForClusters :: [Int] -> [String]
lookForClusters list = map show $ filterSingle $ map (findMultiples list) list

clusters :: String
clusters = unlines $ lookForClusters keithList
     where keithList = map getNumber keithNumbers

keith :: IO ()
keith = putStr $ unlines $ (map (unlines . getLog) keithNumbers) ++ [clusters]
