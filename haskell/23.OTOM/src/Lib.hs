module Lib
    ( addMany
    ) where

import Data.Char

limit :: Int
limit = 1000000

sumDigits :: Int -> Int
sumDigits number = sum $ map digitToInt $ show number

addThemUp :: [Int] -> Int
addThemUp numbers = sum $ map sumDigits numbers

addMany :: IO ()
addMany = putStrLn $ show $ addThemUp [1..limit]
