module Lib (sunflower) where

import System.Environment

fibonacci count = reverse $ intFibo [1,1] $ count-2
    where intFibo :: [Int] -> Int -> [Int]
          intFibo (z:y:x) c 
              | c <= 0    = z:y:x
              | otherwise = intFibo (z+y:z:y:x) $ c-1

getNumber :: [String] -> Int
getNumber []     = 0
getNumber (x:xs) = read x

sunflower :: IO ()
sunflower = do
    args <- getArgs
    putStrLn $ show $ fibonacci $ getNumber args
