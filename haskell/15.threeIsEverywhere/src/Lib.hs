module Lib (three) where

toFloat i      = fromIntegral i :: Float
contains3 n    = elem '3' $ show n
containing3 sq = length $ filter contains3 sq
percentage sq  = ( toFloat (containing3 sq) ) / ( toFloat (length sq) )
getSeq n       = [1..(10 ^ n)]

three :: IO ()
three = putStr $ unlines $ map (show . percentage . getSeq) [1..7]
