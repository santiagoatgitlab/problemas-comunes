module Main where

import System.Random
import Data.Time
import Control.Monad.Writer

diceNumber :: Int
diceNumber = 5 

rollDice :: ([Int], StdGen) -> ([Int], StdGen)
rollDice (dice, gen) 
    | length dice == diceNumber = (dice, gen)
    | otherwise = let (oneMoreDie, nextGen) = randomR (1,6) gen
                  in rollDice (oneMoreDie:dice, nextGen)

getPicoSeconds :: UTCTime -> Int
getPicoSeconds time = div (read $ formatTime defaultTimeLocale "%q" time) 1000

isYahtzee :: [Int] -> Bool
isYahtzee (x:xs) = and $ map (\d -> x == d) xs

rollYahtzee :: StdGen -> Writer [String] ([Int])
rollYahtzee gen = let (dice, nextGen) = rollDice ([], gen)
                       in if (isYahtzee dice)
                          then do
                             tell [ "final: " ++ (show dice) ]
                             return dice
                          else do
                             tell [ "current roll: " ++ (show dice) ]
                             rollYahtzee nextGen

getYahtzeeResult :: StdGen -> String
getYahtzeeResult gen = unlines $ snd $ runWriter $ rollYahtzee gen

main :: IO ()
main = do
    time <- getCurrentTime
    writeFile "yahtzee.log" $ getYahtzeeResult $ mkStdGen $ getPicoSeconds time
