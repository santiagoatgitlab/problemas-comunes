module Lib (thirtySeven) where

import Data.Char
import System.Environment

multiplex :: Int -> Int -> Int
multiplex n times = read (map intToDigit $ replicate times n)

parseArg :: String -> IO Int
parseArg s
    | and $ map isNumber s = return $ read s
    | otherwise = error "Please enter an integer number"

divide :: Int -> Int -> Double
divide n1 n2 = let toDouble n = read (show n) :: Double
               in (toDouble n1) / (toDouble n2)

thirtySeven :: IO ()
thirtySeven = do
    (x:xs) <- getArgs
    i <- parseArg x
    putStrLn $ show $ map (\n -> divide (multiplex n i) (n*i)) [1..9]
