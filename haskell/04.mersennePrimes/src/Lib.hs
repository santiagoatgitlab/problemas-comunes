module Lib (mersenne) where

import Primes

getMersenneNumber n = 2^n - 1
getMersennePrimes l = filter isPrime $ map getMersenneNumber l
makePerfect mp      = (mp * (mp + 1)) `div` 2
getPerfectNumbers l = map makePerfect $ getMersennePrimes l

mersenne :: IO ()
mersenne = let mersennePrimes = getMersennePrimes [1..20]
           in putStr $ unlines [
                "Mersenne primes: "
                ,(show mersennePrimes)
                ,"Perfect numbers primes: "
                ,(show $ map makePerfect mersennePrimes)
                ]
