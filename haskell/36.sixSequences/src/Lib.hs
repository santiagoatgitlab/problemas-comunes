module Lib
    ( addToSequence
    ) where

type Limit   = Int

addToSequence :: [Int] -> Int -> Limit -> [Int]
addToSequence seq number limit
    | (last seq) == limit = seq
    | otherwise           = let newSeq = seq ++ replicate (seq !! (number-1)) number
                             in addToSequence newSeq (number+1) limit
