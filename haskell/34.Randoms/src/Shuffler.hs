module Shuffler (operate, swap, digit) where

digit :: Int -> Int
digit n = mod n 10

operate :: [Int] -> [Int]
operate l = shift $ map (\n -> l !! (digit n)) l
            where shift l = (last l):(init l)

swap :: Int -> [Int] -> [Int]
swap n l = let n2 = digit (n+3)
               (d1,d2) = if n < n2 then (n,n2) else (n2,n)
               (m1,(x1:s3)) = splitAt d2 l
               (s1,(x2:s2)) = splitAt d1 m1 
           in s1 ++ [x1] ++ s2 ++ [x2] ++ s3

shuffle :: Int -> [Int] -> [Int]
shuffle n l
    | n <= 0 = l
    | otherwise = shuffle (n-1) (operate l)
