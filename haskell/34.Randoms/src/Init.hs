module Init (numberSeed) where

import Data.Char

inits :: [[Int]]
inits = [
    [0,2,6,7,3,5,8,9,4,1],
    [3,0,2,6,7,8,9,4,1,5],
    [7,3,0,2,6,9,4,1,5,8],
    [6,7,3,0,2,4,1,5,8,9],
    [2,6,7,3,0,1,5,8,9,4],
    [5,8,9,4,1,0,2,6,7,3],
    [8,9,4,1,5,3,0,2,6,7],
    [9,4,1,5,8,7,3,0,2,6],
    [4,1,5,8,9,6,7,3,0,2],
    [1,5,8,9,4,2,6,7,3,0]
    ]

transform :: [Int] -> [Int] -> [Int]
transform l1 l2 = foldl (\acc cur -> (l2 !! cur):acc) [] l1

numberSeed :: Int -> [Int]
numberSeed n = reverse $ foldl1 (\l1 l2 -> transform l1 l2) $ map (\d -> (inits !! d)) digits 
               where digits = map digitToInt $ show n
