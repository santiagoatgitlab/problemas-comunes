module Generator (generate) where

import Init
import Shuffler
import Data.Char
import Control.Monad.Writer

type Result  = Int
type Seed    = Int
type Max     = Int
type Pointer = Int
type Base    = [Int]
type Partial = String
data Step    = Step Base Partial Pointer Max Int

engine :: Step -> Writer [String] (Result,Seed)
engine (Step base partial pointer maxi originalSeed)
    | (length partial) == (length $ show maxi) = 
                  let result    = round $ (toFloat number) / divider * (toFloat (maxi+1))
                      number    = read partial
                      (seed,_)  = fst $ runWriter $ engine (Step base [] 5 99999999 originalSeed)
                      divider   = 10 ^ (length $ show maxi)
                      toFloat n = fromIntegral n :: Float
                  in  do
                       tell [ "base " ++ (show base) ++ " pointer " ++ (show pointer)]
                       tell [ "----- end number -----" ]
                       return (result,seed)
    | otherwise = do
                   tell [ "base " ++ (show base) ++ " pointer " ++ (show pointer)]
                   engine (Step nextBase nextPartial nextPointer maxi originalSeed)
                  where nextBase    = operate base
                        nextPartial = (intToDigit $ base !! pointer):partial
                        nextPointer = mod (pointer+1) 10

generate :: Max -> Seed -> ((Result,Seed),[String])
generate maxi seed = runWriter $ engine (Step (numberSeed seed) "" 0 maxi seed)
