module Base (base) where

import Data.Char
origs = [137..147]

laSequence :: [Int]
laSequence = [0..9]

adders :: Int -> [Int]
adders n = [ n * m | m <- [1..10] ]

folder :: [Int] -> Int -> [Int]
folder (x:xs) cur = (x + cur + 1):x:xs

indexes :: Int -> [Int]
indexes n = tail $ reverse $ map (\i -> mod i 10) $ foldl folder [-1] (adders n)

getDigits :: [Int] -> [Int] -> [Int]
getDigits l seq
    | (length seq) >= 10 = seq
    | otherwise = getDigits is ((nextNumber i):seq)
                      where (i:is) = l
                            nextNumber n
                              | not $ elem (laSequence !! n) seq = (laSequence !! n)
                              | otherwise = nextNumber (nextIndex n)
                                            where nextIndex nu
                                                      | nu == 9 = 0
                                                      | otherwise = nu+1

base :: Int -> [Int]
base n = getDigits (indexes n) []
