module Lib (randoms) where

import Generator
import Data.List
import Init

randoms :: IO ()
randoms = putStr $ unlines $ map (show . fst) $ map (generate 99999999) [247..267]

toLines :: ((Int, Int), [String]) -> String
toLines (t,l) = unlines $ (show t):l
