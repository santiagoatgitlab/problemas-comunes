import qualified Data.List as List

isDiv n p       = mod n p == 0
isPrime pc 
    | pc < 2    = False
    | otherwise = not $ any (\n -> isDiv pc n) [2..(ceiling $ sqrt $ toFloat pc)]
invert n        = read ( reverse $ show n ) :: Int
isPalyndrom pc  = pc == invert pc 
inversionAdd a  = a + invert a
filterPrime     = filter isPrime
toFloat i       = (fromIntegral i) :: Float

isPrimeCandidatefy primes = (\num -> not $ any (\prime -> num /= prime && isDiv num prime) primes) 
getCandidates             = filter . isPrimeCandidatefy 

firstOrderPrimes  = filter isPrime [2..99]
secondOrderPrimes = filter isPrime $ getCandidates firstOrderPrimes [2..8888]

getWodallNumber a = a * (2 ^ a) - 1
getWodallPrimes l = filterPrime $ getCandidates secondOrderPrimes $ map getWodallNumber l    

sumFirstPlPr = sum $ take 3 $ filterPrime $ filter isPalyndrom [100..999]
smaller      = head $ List.sort $ filterPrime $ map inversionAdd $ filterPrime [2..1000]
