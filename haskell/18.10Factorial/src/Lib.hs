module Lib (fact) where

import System.Environment
import Data.Char

factorial :: Integer -> Integer
factorial n
    | n == 1 = 1
    | n > 1 = n * (factorial $ n-1)
    | otherwise = error "the number is not a positive integer"

getInteger :: [String] -> Integer
getInteger l
    | l == [] = displayError
    | not $ and $ map isNumber $ head l = displayError
    | otherwise = read $ head l :: Integer
        where displayError = error "Please enter an integer number"

toFloat :: Integer -> Float
toFloat i = fromInteger i

minute n = (/) n 60
hour   n = (/) n 60
day    n = (/) n 24
week   n = (/) n 7

getSecondsAndWeeks :: Integer -> (Integer,Float)
getSecondsAndWeeks n = (seconds, weeks)
                       where seconds = factorial n
                             weeks   = (week . day . hour . minute) $ toFloat seconds

fact :: IO ()
fact = do
    args <- getArgs
    putStrLn $ show $ getSecondsAndWeeks $ getInteger args
