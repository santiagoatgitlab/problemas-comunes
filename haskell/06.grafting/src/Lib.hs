module Lib (grafting) where

firstN sq = read (takeWhile (\c -> c /= '.') $ show sq) :: Float

isGrafting n = compareToSqrt n (sqrt n)
    where compareToSqrt n sq 
                        | n > (firstN sq) = compareToSqrt n (sq*10)
                        | n < (firstN sq) = False
                        | otherwise       = True


grafting :: IO ()
grafting = putStrLn $ unwords $ map show $ filter isGrafting [1..20000]
