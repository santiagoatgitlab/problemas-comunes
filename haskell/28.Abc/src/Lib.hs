module Lib (abc) where

import Numberphile
import Data.List

type A = Int
type B = Int
type Factors = [Int]

data Equation = Equation A B deriving Show

radical :: Equation -> Int
radical (Equation a b) = product $ concat $ map (nub . primeFactors) [a,b,c]
               where c = a + b

shareFactors :: (Int,Int) -> Bool 
shareFactors (n1,n2) = elem True $ (==) <$> (primeFactors n1) <*> (primeFactors n2)

radicalIsBiggerThanC :: Equation -> Bool
radicalIsBiggerThanC e = (radical e) > (c e)
              where c (Equation a b) = a + b

equations :: [Equation]
equations = map (\(a,b) -> Equation a b) $ filter (not . shareFactors) $ uniquePairs [2..200]

abc :: IO ()
abc = toLines $ filter (not . radicalIsBiggerThanC) $ equations
