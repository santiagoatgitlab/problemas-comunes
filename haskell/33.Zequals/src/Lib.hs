module Lib (zequals) where

import System.Environment

approximate :: Int -> Int
approximate n =  let factor = read ( ['1'] ++ (replicate ((length $ show n) - 1) '0')) :: Float
                     nFloat = fromIntegral n :: Float  
                  in (*) (round factor) $ round (nFloat / factor) 

argToInt :: String -> Int
argToInt a = read a

getOperation :: (Foldable t, Num a) => String -> t a -> a
getOperation op
    | op == "sum"     = sum
    | op == "product" = product
    | otherwise       = error "first parameter must be a valid operation"

zequals :: IO ()
zequals = do
    args <- getArgs
    putStrLn $ show $ (exact args, approx args)
    where approx args      = approximate $ (operation args) $ map approximate $ numbers args
          exact args       = (operation args) (numbers args)
          numbers (x:xs)   = map argToInt xs
          operation (x:xs) = getOperation x
