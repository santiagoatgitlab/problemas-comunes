module Lib (narcissistics) where

import Data.Char

isNarcissistic n = let nDigits   = length $ show n
                       intPow sd = (digitToInt sd) ^ nDigits 
                   in (==n) $ sum $ map (\sd -> intPow sd) $ show n

narcissistics :: IO ()
narcissistics = putStrLn $ show $ take 8 $ filter isNarcissistic [10..]
