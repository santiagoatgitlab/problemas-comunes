toNDecimals decimals number = let
    toFloat n  = read n :: Float
    totalChars = (+) (decimals+1) $ getPos '.' (show number) 0
        where getPos c (x:xs) n
                  | x == c    = n
                  | otherwise = getPos c (xs) n+1
    in toFloat $ take totalChars $ show number

