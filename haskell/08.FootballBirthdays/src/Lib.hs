module Lib (football) where

import Text.Printf (printf)

toNDecimals number decimals = let
    toFloat n  = read n :: Float
    totalChars = (+) (decimals+1) $ getPos '.' (show number) 0
        where getPos c (x:xs) n
                  | x == c    = n
                  | otherwise = getPos c (xs) n+1
    in toFloat $ take totalChars $ show number

calc n
    | n > 0     = (*) 100 $ (-) 1 $ foldl func 1 initSq
    | otherwise = (-1) 
    where initSq     = [365,364..(366-n)]
          func acc c = acc*(c/365)

football :: IO ()
football = do 
    putStrLn "How many people are there?"
    input <- getLine
    putStrLn $ statement ++ (getNumber input) ++ "%"
    where getNumber input = show $ toNDecimals (calc (read input :: Float)) 2
          statement = "The probability of 2 people sharing birthday is "
