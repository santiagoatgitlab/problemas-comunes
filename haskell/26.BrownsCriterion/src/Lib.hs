module Lib (brownsCriterion) where
import Data.List
import System.Environment

type Card = [Int]
data Number = Number Int [Int]

generateCards :: Int -> [Card]
generateCards cardsNumber = map (\card -> fillCard [card] getNumbers) powersOfTwo
    where getNumbers :: [Number]
          getNumbers = map (\n -> Number n (getPowers [] n)) initNumbers
          fillCard :: Card -> [Number] -> Card
          fillCard card numbers = foldl addToCard card numbers
          addToCard :: Card -> Number -> Card
          addToCard (x:xs) (Number n p)
              | elem x p && x /= n = (x:xs) ++ [n]
              | otherwise          = (x:xs)
          getPowers :: [Int] -> Int -> [Int]
          getPowers p n
              | n == 0    = p
              | otherwise = let (oneMorePower,nextRemainder) = nextPower n
                            in getPowers (oneMorePower:p) nextRemainder
          nextPower :: Int -> (Int,Int)
          nextPower n = let (Just power) = find (<=n) $ reverse powersOfTwo
                        in (power,n-power)
          powersOfTwo :: [Int]
          powersOfTwo = map (\n -> 2 ^ n) [0..cardsNumber-1]
          initNumbers :: [Int]
          initNumbers = [1..maxNumber]
          maxNumber :: Int
          maxNumber = (2 ^ cardsNumber) - 1

brownsCriterion :: IO ()
brownsCriterion = do 
    cardsNumber <- getArgs
    putStr $ unlines $ map show $ generateCards (read (cardsNumber !! 0) :: Int)
