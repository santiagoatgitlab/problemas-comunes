module Lib (sexies) where

import Primes

primes = filter isPrime [1..10000]
lambda :: [(Int,Int)] -> Int -> [(Int,Int)]
lambda acc cur = if elem (cur+6) primes then (cur,cur+6):acc else acc
sexy = reverse $ foldl lambda [] primes

sexies :: IO ()
sexies = putStrLn $ show $ take 50 sexy
