-- Numberphile video: 6000000 and Abel Prize
import Primes

n = 551
p = 4
k = 5

primes :: [Int]
primes = take 1000 $ filter isPrime [1..]

isSeqPossible :: Int -> Int -> Bool
isSeqPossible n f = elem n primes && isSeqPossible (f + n)
