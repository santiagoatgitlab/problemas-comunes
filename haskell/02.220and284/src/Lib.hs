module Lib (tttef) where

type Found = [Int]
type Pairs = [Int]

getDivisors n = filter (\dc -> n `mod` dc == 0) [1..(ceiling $ fromIntegral (div n 2))]
amiCandy = sum . getDivisors
getAmi n = let candy = amiCandy n
             in if n == (amiCandy candy) && n /= candy
                  then Just candy
                  else Nothing

getAmicals :: Int -> Found -> Pairs -> Int -> [(Int, Int)]
getAmicals n found pairs limit
    | n > limit = zip found pairs
    | otherwise = if not (elem n pairs) && getAmi n /= Nothing
                    then let Just ami = getAmi n
                          in getAmicals (n+1) (n:found) (ami:pairs) limit
                    else getAmicals (n+1) found pairs limit

pairs first limit = getAmicals first [] [] limit

tttef :: IO ()
tttef = do
    putStr $ unlines $ map show $ reverse $ getAmicals 1 [] [] 8000
