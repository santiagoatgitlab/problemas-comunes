module Lib (taxis) where

import qualified Data.Set as Set
import qualified Data.List as List
cubes   = takeWhile (\n -> n < 99999) $ map (^3) [1..100]
sums    = (+) <$> cubes <*> cubes
numbers = Set.fromList sums
occ     = Set.toList $ Set.map (\n -> (n, length $ List.elemIndices n sums)) numbers

taxis :: IO ()
taxis = putStrLn $ show $ map fst $ filter (\(n,o) -> o > 2) occ
