-- Numberphile video: 6174

import Data.List as List
import Data.Char as Char
import qualified Data.Set as Set

areAllEqual (x:xs) = all (==x) xs
highToLow n        = reverse $ sort n

separate n
    | length n < 2 = (n,n)
    | otherwise    = (reverse $ tail $ reverse n, [head $ reverse n])

toOperand n = Operand (highToLow n) (highToLow n) ""
toExpected n = toExpectedInt (show n,0)

toExpectedInt (n,c)
    | areAllEqual n = -1
    | n == "6174"   = c
    | otherwise     = toExpectedInt (operate (toOperand n), c+1)


data Operand = Operand [Char] [Char] [Char] deriving (Show)

getNextOperand n l a = let getSep = separate $ show $ (read n :: Int) - (digitToInt $ head l)
                           getNum = fst $ getSep
                           getAcc = (snd $ getSep) ++ a
                       in Operand getNum (tail l) getAcc

operate (Operand _ [] acc)     = acc
operate (Operand num list acc) = operate (getNextOperand num list acc)

s = map (\n -> n) [1001..9998]
t = map (\n -> (n,toExpected n)) s
u = filter (\(n,c) -> c == (-1)) t
v = map (\(n,c) -> n) u
w = map (\(n,c) -> c) t
x = Set.fromList w
