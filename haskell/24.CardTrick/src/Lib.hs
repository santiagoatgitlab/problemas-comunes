module Lib ( getCards ) where

import Data.Time
import System.Random

data Suit = Spades | Diamonds | Clubs | Hearts deriving (Show, Eq, Ord)
data Card = Card Suit Int deriving Eq
type Deck = [Card]

instance Show Card where
    show (Card s n)
        | n == 1  = "Ace of " ++ (show s)
        | n == 11 = "Jack of " ++ (show s)
        | n == 12 = "Queen of " ++ (show s)
        | n == 13 = "King of " ++ (show s)
        | otherwise  = (show n) ++ " of " ++ (show s)

getPicoSeconds :: UTCTime -> Int
getPicoSeconds time = div (read $ formatTime defaultTimeLocale "%q" time) 1000

extractElement :: Int -> Deck -> (Deck,Deck)
extractElement pos list = let (h, el:t) = splitAt pos list
                          in (el, h++t)

deck :: Deck
deck = Card <$> [Spades, Diamonds, Clubs, Hearts] <*> [1..13]

shuffle :: StdGen -> Deck -> Deck -> Deck
shuffle gen shuffled ordered
    | ordered == [] = shuffled
    | otherwise     = shuffle nextGen (oneMoreCard:shuffled) remainingOrdered
                      where (oneMoreCard, remainingOrdered) = extractElement number ordered
                            (number, nextGen) = randomR (0,((length ordered)-1)) gen

getCards :: IO ()
getCards = do
    time <- getCurrentTime
    putStr $ unlines $ map show $ shuffle (mkStdGen $ getPicoSeconds time) [] deck
