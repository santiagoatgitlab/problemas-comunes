module Lib (happy) where

import Data.Char
data HappyCandidate = Happy Int [Int] | Sad Int [Int] deriving (Show)

getHappiness (number:prevs)
    | elem number prevs = Sad (last (number:prevs)) prevs
    | number == 1       = Happy (last (number:prevs)) (1:prevs)
    | otherwise         = let newNumber = sum $ map ((^2) . digitToInt) $ show number
                          in getHappiness (newNumber:number:prevs)

isHappy (Sad _ _)   = False
isHappy (Happy _ _) = True

showCandidate (Happy number prevs) = concat [show number," : ",unwords $ map show $ reverse prevs," <",replicate 43 '-']
showCandidate (Sad number prevs)   = show number ++ " : " ++ unwords (map show $ reverse prevs)

candidates = map (\n -> getHappiness [n]) [1..99]
first99    = map showCandidate $ candidates

happy :: IO ()
happy = putStr $ unlines $ map show first99 
