-- Numberphile video: 10!
import System.Environment
import Data.Char

factorial :: Float -> Float
factorial n
    | n == 1 = 1
    | n > 1 = n * (factorial $ n-1)
    | otherwise = error "the number is not a positive integer"

getFloat :: [String] -> Float
getFloat l
    | l == [] = displayError
    | not $ and $ map isNumber $ head l = displayError
    | otherwise = read $ head l :: Float
        where displayError = error "Please enter an integer number"

minute n = (/) n 60
hour   n = (/) n 60
day    n = (/) n 24
week   n = (/) n 7

getSecondsAndWeeks :: Float -> (Float,Float)
getSecondsAndWeeks n = (seconds, weeks)
                       where seconds = factorial n
                             weeks   = (week . day . hour . minute) seconds

main = do
    args <- getArgs
    putStrLn $ show $ getSecondsAndWeeks $ getFloat args
