module Lib (mattsEx) where

import Data.Char

isSuperNarnissistic n = let pow a = a ^ a
                        in n == (sum $ map (pow . digitToInt) $ show n)

mattsEx :: IO ()
mattsEx = putStrLn $ unwords $ map show $ filter isSuperNarnissistic [2..200000]
