module Lib (vampires) where

import Numberphile
import Data.List
import Data.Char

data Vampire = Vampire Int Int Int deriving Show

isVampire :: (Int,Int) -> Bool
isVampire (i,j) = let prod = i * j
                      factorsDigits = sort $ concat $ map ((map digitToInt) . show) [i,j]
                      productDigits = sort $ map digitToInt $ show prod
                   in factorsDigits == productDigits

pair :: Int -> Int -> (Int,Int)
pair i j = (i,j)

addProduct :: (Int,Int) -> Vampire
addProduct (i,j) = Vampire i j (i*j)

prodIs :: Int -> Vampire -> Bool
prodIs n (Vampire _ _ p) = p == n

getProduct :: Vampire -> Int
getProduct (Vampire _ _ p) = p

moreThanOnce :: [Int] -> Int -> Bool
moreThanOnce list n = 1 < (length $ elemIndices n list)

vampires :: IO ()
vampires = toLines $ filter (\(Vampire _ _ p) -> elem p repeated) list
--vampires = toLines list
     where list     = map addProduct $ filter isVampire $ uniquePairs [1..1000]
           repeated = filter (moreThanOnce $ map getProduct list) [1..10000]
