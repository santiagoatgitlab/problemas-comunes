module Lib (internet) where

system = 16
octetLenght = 4
numberOfOctets = 8

addCommas :: String -> String
addCommas s = innerAC "" s
    where innerAC result (x:xs)
             | xs == "" = result ++ [x]
             | mod (length xs) 3 == 0 = innerAC (result ++ [x] ++ ",") xs
             | otherwise = innerAC (result ++ [x]) xs

internet :: IO ()
internet = do
    putStrLn $ addCommas $ show $ (system ^ octetLenght) ^ numberOfOctets
