module Lib
    ( brownNumbers
    ) where

factorial :: Int -> Int
factorial n = foldl1 (*) [1..n]

isBrownPair :: (Int,Int) -> Bool
isBrownPair (sn,fn) = (factorial fn) + 1 == sn ^ 2

toPair :: Int -> Int -> (Int,Int)
toPair n1 n2 = (n1,n2)

brownNumbers :: IO ()
brownNumbers = putStr $ unlines $ map show $ filter isBrownPair $ toPair <$> [1..999] <*> [1..9]
