module Main where

import Lib
import Primes
import Data.List

data Pair = Pair { getNumber :: Int, getPC :: Int } deriving Eq

instance Show Pair where
    show (Pair x primeCount) = show x ++ ": " ++ show primeCount

instance Ord Pair where
    compare (Pair _ pc1) (Pair _ pc2) = compare pc2 pc1

x -: f = f x

countPrimes :: Int -> Int
countPrimes x = map (\n -> ulamFunction x n) [1..1000] -: filter isPrime -: length

main :: IO ()
main = map (\n -> Pair n (countPrimes n)) [1..100] -: sort -: take 25 -: map show -: unlines -: putStr
