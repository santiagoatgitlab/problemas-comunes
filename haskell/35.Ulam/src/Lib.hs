module Lib
    ( ulamFunction
    ) where

ulamFunction :: Int -> Int -> Int
ulamFunction x n = n^2 - n + x
