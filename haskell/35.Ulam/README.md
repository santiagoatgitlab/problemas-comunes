# ulam

This program is based on the numberphile video "41 and more Ulam\'s spiral"

It programs shows how many prime numbers are generated from using the formula
(n^2 - n + x) for different values of x and n.  

Given x, it calculates how many cases of n (with n in a range from 1..1000) return a prime number.

It displays a list of pairs with x and the amount of corresponding primes
