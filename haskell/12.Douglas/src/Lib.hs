module Lib (douglas) where

import Primes
import Data.Char

toFloat i = (fromIntegral i) :: Float

isPronic :: Int -> Bool
isPronic n = or $ foldl (\(pv,isP) cv -> (cv,(pv*cv==n)||isP)) (1,False) [2 .. (div n 2) ]

addInverse :: [Int] -> Int
addInverse []     = 0
addInverse (n:[]) = n
addInverse l      = let divider n = div (last l) n
                    in foldl1 (+) $ map divider l

isPPP :: Int -> Bool
isPPP n = addInverse (primeFactors n ++ [n]) == n

isHarshad :: Int -> Bool
isHarshad n = (mod n $ foldl1 (+) $ map digitToInt $ show n) == 0

pronic  = (++) "First 7 pronics: " $ show $ take 7 $ filter isPronic [ 0 .. ]
ppp     = (++) "First 5 primary pseudo-perfect: " $ show $ take 5 $ filter isPPP [ 2 .. ]
harshad = (++) "First 25 harshad: " $ show $ take 25 $ filter isHarshad [ 1 .. ]

douglas :: IO ()
douglas = putStr $ unlines [pronic, ppp, harshad]
