module Numberphile
    ( isPrime
    , primeFactors
    , orderPair
    , nubSorted
    , uniquePairs
    , toLines
    ) where

import Data.List

toFloat i       = (fromIntegral i) :: Float
isPrime pc 
    | pc < 2    = False
    | otherwise = not $ any (\n -> mod pc n == 0 && n /= pc) [2..(ceiling $ sqrt $ toFloat pc)]

firstDivider :: Int -> Int
firstDivider np 
    | np == 1   = 1
    | otherwise = head $ filter (\n -> mod np n == 0) [2..(ceiling $ sqrt $ toFloat np)]

primeFactors :: Int -> [Int]
primeFactors a 
    | a < 2     = error "function primeFactors doesn't accept any number smaller than 2"
    | isPrime a = [a]
    | otherwise = primeFactors factor1 ++ primeFactors factor2
        where factor1 = firstDivider a
              factor2 = round $ (toFloat a) / (toFloat factor1)

orderPair :: (Int,Int) -> (Int,Int)
orderPair (n1,n2)
    | n1 > n2   = (n2,n1)
    | otherwise = (n1,n2)

nubSorted :: Eq a => [a] -> [a]
nubSorted l = reverse (foldl join [] l)
    where join [] a     = (a:[])
          join (x:xs) a = if a == x then (x:xs) else (a:x:xs)

pairs :: [Int] -> [(Int,Int)]
pairs list = (\n1 n2 -> (n1,n2)) <$> list <*> list

uniquePairs :: [Int] -> [(Int,Int)]
uniquePairs list = nubSorted $ sort $ map orderPair $ pairs list

toLines :: Show a => [a] -> IO ()
toLines list = putStr $ unlines $ map show list
