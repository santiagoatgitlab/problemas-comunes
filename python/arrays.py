from functools import reduce
sequence = [ 81, 14, 1, 104, 68, 56, 89, 4 ]
print(list(filter(lambda num: num % 2 == 0, sequence)))
print(list(filter(lambda num: num % 2 != 0, sequence)))
print(reduce(lambda acc, num: acc + num, sequence))
print(sorted(sequence))
print(sorted(sequence, reverse=True))
