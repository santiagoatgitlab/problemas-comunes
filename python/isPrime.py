import sys
num     = int(sys.argv[1])
isPrime = lambda num : not any(num % x == 0 for x in range(2,num))
print(isPrime(num))
