import sys
fromNumber  = int(sys.argv[1])
toNumber    = int(sys.argv[2])
isPrime     = lambda num : not any(num % x == 0 for x in range(2,num))
print(list(filter(lambda num : isPrime(num), range(int(fromNumber),int(toNumber)+1))))
