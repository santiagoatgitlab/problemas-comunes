//! # Numberphile
//! 
//! Numberphile is a collection of utilities  
//! to help solve the numberphile exercises

/// Returns true if the given number is prime, false otherwise
///
/// # Examples
/// 
/// ```
/// let arg = 29;
/// let result = numberphile::is_prime(arg);
/// 
/// assert!(result);
/// ``` 
pub fn is_prime(number: u32) -> bool {
    let rounded_sqrt = (number as f64).sqrt() as u32;
    number > 1 && !(2..(rounded_sqrt+1)).any(|div| number % div == 0 )
}

/// Returns a list of a the first n amount of mersenne primes
///
/// # Examples
/// 
/// ```
/// let arg = 5;
/// let result = numberphile::get_mersenne_primes(arg);
/// 
/// assert_eq!(result[0], 3);
/// assert_eq!(result[1], 7);
/// assert_eq!(result[2], 31);
/// assert_eq!(result[3], 127);
/// assert_eq!(result[4], 8191);
/// ```                
pub fn get_mersenne_primes(limit: usize) -> Vec<u32> {

    let mut mersenne_primes: Vec<u32> = Vec::new();
    let mut current_mersenne;
    let mut count = 1;
    loop {
        current_mersenne = 2_i32.pow(count) as u32 - 1;
        if is_prime(current_mersenne) {
            mersenne_primes.push(current_mersenne);
        }
        if mersenne_primes.len() == limit { break; }
        count += 1;
    }

    mersenne_primes
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_prime_returns_true_when_given_a_prime() {
        assert!(is_prime(2));
    }

    #[test]
    fn is_prime_returns_false_when_given_a_not_prime() {
        assert!(!is_prime(1));
    }
}
