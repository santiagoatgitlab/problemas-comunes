fn main() {
    let mut already_used: Vec<u32> = Vec::new();
    for number in 1..8000 {
        if !already_used.contains(&number) {
            match get_friend(number) {
                Some(friend) => {
                    already_used.push(friend);
                    println!("({},{})", number, friend);
                },
                None => ()
            };
        }
    }
}

fn get_friend(number: u32) -> Option<u32> {
    let possible_friend: u32 = get_divider_sum(number);
    let possible_friend_dividers_sum: u32 = get_divider_sum(possible_friend);
    if possible_friend != number && possible_friend_dividers_sum == number{
        return Some(possible_friend)
    }
    None
}

fn get_divider_sum(number: u32) -> u32 {
    let mut sum = 0;
    for i in 1..number {
        if number % i == 0 {
            sum = sum + i
        }
    }
    sum
}
