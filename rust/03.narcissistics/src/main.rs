fn main() {
    let narcissistics: Vec<u32> = (10..60000).filter(|n| {
        is_narcissistic(*n)
    }).collect();
    println!("{:?}", narcissistics);
}

fn is_narcissistic(number: u32) -> bool {
    let number_chars = number.to_string();
    let number_of_digits = number_chars.len() as u32;
    let power = |c:char| (c.to_digit(10).unwrap() as u32).pow(number_of_digits);
    number_chars.chars()
        .map(|c| power(c) )
        .sum::<u32>() == number
}
