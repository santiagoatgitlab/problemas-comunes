fn main() {
    let mut bad_numbers = String::from("Bad numbers:");
    for i in 1111..9999 {
        let mut next_number = i;
        let mut display = String::from(next_number.to_string());
        let mut counter = 0;
        loop {
            counter = counter + 1;
            next_number = operate(next_number);
            if next_number != 0 && next_number != 999 {
                display.push_str(" -> ");
                display.push_str(&next_number.to_string());
            }
            else{
                break;
            }
            if next_number == 6174 { break };
        }
        if next_number == 6174 {
            println!("{}", display);
        }
        else {
            bad_numbers.push_str(" | ");
            bad_numbers.push_str(&i.to_string());
        }
    }
    println!("{}", bad_numbers);
}

fn operate(prev_number: u32) -> u32 {
    let mut input = Vec::new();
    for i in prev_number.to_string().chars() {
        input.push(i);
    }
    input.sort();
    let mut reversed_input = Vec::new();
    for i in &input {
        reversed_input.push(*i);
    }
    reversed_input.reverse();
    let input: u32 = join_vec(&input).parse().unwrap();
    let reversed_input: u32 = join_vec(&reversed_input).parse().unwrap();
    let new_number = reversed_input - input;
    new_number
}

fn join_vec(vec: &Vec<char>) -> String{
    let mut result = String::new();
    for i in vec {
        result.push(*i);
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn join_vec_recieves_a_vec_and_returns_a_string_with_its_chars() {
        let vec: Vec<char> = vec!['1','4','9','8'];
        let number = "1498";
        assert_eq!(join_vec(&vec),number);
    }

    #[test]
    fn operate_receives_a_number_and_operates_it() {
        assert_eq!(operate(9218), 8532);
        assert_eq!(operate(2984), 7353);
    }

    #[test]
    fn operate_receives_0_for_all_equal_digits() {
        assert_eq!(operate(3333), 0);
        assert_eq!(operate(4444), 0);
    }
}
