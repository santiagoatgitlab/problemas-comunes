use std::env;
use std::process;
use numberphile;

fn main() {
    let limit = get_limit_from_arguments();

    let perfect_numbers: Vec<u64> = numberphile::get_mersenne_primes(limit)
        .into_iter()
        .map(|num| {
            ((num * (num + 1)) / 2) as u64
        }).collect();
    dbg!(perfect_numbers);
}

fn get_limit_from_arguments() -> usize {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        eprintln!("[PARSING ERROR] please enter the amount of perfect numbers you expect");
        process::exit(1);
    }

    let limit: usize = args[1].parse().unwrap_or_else(|err| {
        eprintln!("[PARSING ERROR] {}", err);
        process::exit(1);
    });
    limit
}
