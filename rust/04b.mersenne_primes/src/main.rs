use std::env;
use std::process;
use numberphile::*;

fn main() {
    let limit = get_limit_from_args();

    let mut mersenne_primes: Vec<u32> = Vec::new();
    let mut current_mersenne;
    let mut count = 1;
    loop {
        current_mersenne = 2_i32.pow(count) as u32 - 1;
        if is_prime(current_mersenne) {
            mersenne_primes.push(current_mersenne);
        }
        if mersenne_primes.len() == limit { break; }
        count += 1;
    }

    dbg!(mersenne_primes);
}

fn get_limit_from_args() -> usize {
    let args: Vec<String> = env::args().collect();
    let limit = if args.len() > 1 { &args[1] } else {
        eprintln!("[PARSING ERROR] Please enter the amount of mersenne numbers");
        process::exit(1);
    };

    let limit: usize = limit.parse().unwrap_or_else(|_| {
        eprintln!("[PARSING ERROR] Argument must be an interger number");
        process::exit(1);
    });
    limit
}
