const sequence = [ 81, 14, 1, 104, 68, 56, 89, 4 ]
console.log (sequence.filter( num => num % 2 == 0 ))
console.log (sequence.filter( num => num % 2 != 0 ))
console.log (sequence.reduce( (acc, num) => acc + num ))
console.log (sequence.sort( (a, b) => a > b ? 1 : -1 ))
console.log (sequence.sort( (a, b) => a < b ? 1 : -1 ))
