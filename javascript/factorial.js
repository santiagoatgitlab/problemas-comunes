// Numberphile video: what do 5, 13, and 563 have in common?
const getSeq    = (from, to) => Array.from(Array(to-from+1), (_, key) => key + from )
const isPrime   = num => !getSeq(2,Math.floor(Math.max(2,num)/2)).some( pDivisor => num % pDivisor == 0 )

const factorial = num => getSeq(1,num).reduce( (acc,num) => acc * num )
const primes    = getSeq(2,20).filter(isPrime)

function divideUntilDecimals([ bigNumber, num, count ]){
    if (bigNumber % num == 0){
        const next = bigNumber / num
        count++
        return divideUntilDecimals( [ next, num, count ] )
    }
    return count
}

console.log(primes.filter( num => divideUntilDecimals( [ factorial(num-1) + 1, num, 0 ] ) > 1 ))
