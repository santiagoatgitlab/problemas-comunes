// Numberphile video: 63 and -7/4 are special
import { getSeq, isPrime } from './primes.js'
const formula = num => Math.pow(num, 2) - 3
const getMinorPrimes = num => getSeq(2,num).filter(isPrimes)
const findDivisorsBetweenMinorPrimes = num => z

function passResultIntoFormulaUntilNumber(seq, end){
    const next = formula(seq[seq.length-1])
    if (next >= end){
        return seq
    }
    seq.push(next)
    return passResultIntoFormulaUntilNumber(seq, end)
}

passResultIntoFormulaUntilNumber([0], 200200200)
    .forEach( num => console.log(num))
