const getSeq = (from, to) => Array.from(Array(to-from+1), (_, key) => key + from )
const isPrime = num => { 
    return num > 1 &&
    !getSeq(2,Math.floor(Math.sqrt(num))).some( pDivisor => num % pDivisor == 0 )
}
const primes           = getSeq(2,999).filter(isPrime)
const isDividable      = (dividend, divisor) => dividend % divisor == 0
const isPrimeCandidate = num => !primes.some(prime => num != prime && isDividable(num, prime))

export { getSeq, isPrime, isDividable, isPrimeCandidate }
