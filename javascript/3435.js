// Numberphile video: 3435 - Jan 13, 2012

const pow = n => Math.pow(n,n)
const isSuperNarcissistic = n => Array.from(String(n))
                                      .map( a => pow(Number(a)) )
                                      .reduce( (acc,digit) => acc + digit )
                                      === n

for (let i=2; i<200000; i++) if (isSuperNarcissistic(i)) console.log(i)
