// Numberphile video: 153 and Narcissistic numbers - Jun 2, 2012

const nDigits = n      => String(n).length
const intPow  = (sd,p) => Math.pow(Number(sd),p)

const isNarcissistic = n => Array.from(String(n))
                                 .map( sd => intPow(sd,nDigits(n)))
                                 .reduce( (sum, current) => sum + current )
                                 == n

// for (let i=1; i<100000; i++) if (isNarcissistic(i)) console.log(i)
for (let i=10; i<100000; i++) if (isNarcissistic(i)) { console.log(i); break }

