const [from, to] = Deno.args.map( arg => Number(arg) )
const getSeq = (from, to) => Array.from(Array(to-from+1), (_, key) => key + from )
const isPrime = num => num != 1 && !getSeq(2,Math.floor(Math.max(2,num)/2)).some( pDivisor => num % pDivisor == 0 )
export { getSeq, isPrime }
