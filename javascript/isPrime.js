const [num] = Deno.args.map( arg => Number(arg) )
const seq = Array.from(Array(Math.ceil(Math.sqrt(num))-1 )).map( (item, key) => key + 2 )
console.log(!seq.some( pDivisor => num % pDivisor == 0 ))
