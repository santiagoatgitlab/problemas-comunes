// Numberphile video: 383 is cool
import { getSeq, isPrime, isDividable } from './primes.js'

const isPrimeCandidatefy = primes => {
    return num => !primes.some(prime => num != prime && isDividable(num, prime))
}

console.time()
const firstOrderPrimes = getSeq(2,99).filter(isPrime)
console.timeEnd()
console.time()
const secondOrderPrimes = getSeq(2,8888)
                            .filter(isPrimeCandidatefy(firstOrderPrimes))
                            .filter(isPrime)
console.timeEnd()

const startWithOdd     = num => Number(String(num)[0]) % 2 != 0
const invert           = num => Number(Array.from(String(num)).reverse().join(''))
const isPalyndrom      = num => num == invert(num)
const inversionAdd     = num => num + invert(num)
const getWoodallNumber = num => num * Math.pow(2, num) - 1
const getWoodallPrimes = seq => seq.map(num => getWoodallNumber(num))
                                        .filter(isPrimeCandidatefy(secondOrderPrimes))
                                        .filter(isPrime)

console.time()
const woodallPrimes = getWoodallPrimes(getSeq(1,63)) // after 63 generated numbers 

const sum = getSeq(100,999)
        .filter(startWithOdd) // only added for efficiency
        .filter(isPalyndrom)
        .filter(isPrime)
        .slice(0,3)
        .reduce( (addition,num) => addition+num )

const smalller = getSeq(2,1000)
        .filter(isPrime)
        .map(inversionAdd)
        .filter(isPrime)
        .sort( (a,b) => a > b ? 1 : -1 )
        [0]

console.log(woodallPrimes, sum, smalller)
console.timeEnd()
