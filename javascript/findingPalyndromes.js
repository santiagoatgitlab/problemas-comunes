// Numberphile video: What's special about 196?
const getSeq = (from, to) => Array.from(Array(to-from+1), (_, key) => key + from )

const seq = getSeq(1,195)

const invert            = num => Number(Array.from(String(num)).reverse().join(''))
const addMirrored       = num => num + invert(num)
const obtainBiggerCount = (last,current) => last.count >= current.count ? last : current
const getAllCounts      = (list,count) => list.add(count)


function addMirroredUntilItIsPalyndrom([ original, num, count ]){
    const next = addMirrored(num)
    if (next == invert(next)){
        return [ original, next, count ]
    }
    return addMirroredUntilItIsPalyndrom([ original, next, ++count ])
}

const result = seq.map( num => addMirroredUntilItIsPalyndrom([ num, num, 1 ]))

result.filter( ([,,count]) => count > 6)
    .forEach(elem => console.log(elem))

console.log(
    result.map( ([num,,count]) => ({ num, count }) )
        .reduce(obtainBiggerCount)
)

console.log(
    Array.from(result.map( ([,,count]) => (count) )
        .reduce(getAllCounts,new Set())).sort( (a,b) => a > b ? 1 : -1)
)
