import { getSeq, isPrime } from './primesInSeq.js'
const to = 100*10
const primes = getSeq(1,to).filter(isPrime)
const getNextDecimal = (dend,sor) => [ Math.floor(dend*10/sor), dend*10 % sor ]

function divideUntilPeriodEnds(dividend, divisor, decimals, allQuotients){
    const[ result, quotient ] = getNextDecimal(dividend,divisor)
    if (allQuotients.includes(quotient)){
        return decimals
    }
    allQuotients.push(quotient)
    decimals.push(result)
    return divideUntilPeriodEnds(quotient,divisor,decimals,allQuotients)
}

const getPeriod = num => divideUntilPeriodEnds(1, num, [], [])
const isCyclic  = num => num > 2 && getPeriod(num).length == num-1

const cyclics       = primes.filter( num => isCyclic(num) )
const percentage    = 100/primes.length*cyclics.length
console.log(cyclics, percentage)
